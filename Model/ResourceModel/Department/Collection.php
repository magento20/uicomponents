<?php
namespace Assaka\Uicomponents\Model\ResourceModel\Department;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    protected $_idFieldName = \Assaka\Uicomponents\Model\Department::DEPARTMENT_ID;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Assaka\Uicomponents\Model\Department', 'Assaka\Uicomponents\Model\ResourceModel\Department');
    }

}