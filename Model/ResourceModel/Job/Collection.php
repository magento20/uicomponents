<?php
namespace Assaka\Uicomponents\Model\ResourceModel\Job;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    protected $_idFieldName = \Assaka\Uicomponents\Model\Job::JOB_ID;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Assaka\Uicomponents\Model\Job', 'Assaka\Uicomponents\Model\ResourceModel\Job');
    }

}