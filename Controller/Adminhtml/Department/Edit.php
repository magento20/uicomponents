<?php
namespace Assaka\Uicomponents\Controller\Adminhtml\Department;

use Assaka\Uicomponents\Controller\Adminhtml\Department;

class Edit extends Department
{
    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Assaka_Uicomponents::department')
            ->addBreadcrumb(__('Department'), __('Department'))
            ->addBreadcrumb(__('Manage Departments'), __('Manage Departments'));
        return $resultPage;
    }

    /**
     * Edit Department
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {

//        die('edit');

        $id = $this->getRequest()->getParam('id');
        $departmentModel = $this->_departmentFactory->create();

        // If you have got an id, it's edition
        if ($id) {

            $departmentModel->load($id);
            if (!$departmentModel->getId()) {
                $this->messageManager->addError(__('This department not exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
            $this->_coreRegistry->register('uicomponents_department', $departmentModel);
        }

        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $departmentModel->setData($data);
        }

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Department') : __('New Department'),
            $id ? __('Edit Department') : __('New Department')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Departments'));
        $resultPage->getConfig()->getTitle()
            ->prepend($departmentModel->getId() ? $departmentModel->getName() : __('New Department'));

        return $resultPage;
    }
}