<?php
namespace Assaka\Uicomponents\Controller\Adminhtml\Department;

use Assaka\Uicomponents\Controller\Adminhtml\Department;

class Index extends Department
{
    const ADMIN_RESOURCE = 'Assaks_Uicomponents::department';

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Assaks_Uicomponents::department');
        $resultPage->addBreadcrumb(__('Jobs'), __('Jobs'));
        $resultPage->addBreadcrumb(__('Manage Departments'), __('Manage Departments'));
        $resultPage->getConfig()->getTitle()->prepend(__('Department'));

        return $resultPage;
    }
}