<?php
namespace Assaka\Uicomponents\Controller\Adminhtml\Department;

use Assaka\Uicomponents\Controller\Adminhtml\Department;

class Add extends Department
{

    public function execute()
    {

       $this->_forward('edit');
    }
}