<?php
namespace Assaka\Uicomponents\Controller\Adminhtml\Department;

use Assaka\Uicomponents\Controller\Adminhtml\Department;

class Delete extends Department
{

    /**
     * Edit Department
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {


        $id = (int) $this->getRequest()->getParam('id');
        /** @var $newsModel \Assaka\Mygrid\Model\Department */
        $departmentModel = $this->_departmentFactory->create();

        if ($id) {

            $departmentModel->load($id);

            // Check this news exists or not
            if (!$departmentModel->getId()) {
                $this->messageManager->addError(__('This department no longer exists.'));
            } else {
                try {
                    // Delete news
                    $departmentModel->delete();
                    $this->messageManager->addSuccess(__('The department has been deleted.'));

                    // Redirect to grid page
                    $this->_redirect('*/*/');
                    return;
                } catch (\Exception $e) {
                    $this->messageManager->addError($e->getMessage());
                    $this->_redirect('*/*/edit', ['id' => $departmentModel->getId()]);
                }
            }
        }
    }
}