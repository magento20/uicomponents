<?php
namespace Assaka\Uicomponents\Controller\Adminhtml\Department;

use Assaka\Uicomponents\Controller\Adminhtml\Department;

class Save extends Department
{
    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            /** @var \Assaka\Uicomponents\Model\Department $departmentModel */
            $departmentModel = $this->_departmentFactory->create();

            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $departmentModel->load($id);
            }

            $departmentModel->setData($data);

            $this->_eventManager->dispatch(
                'jobs_department_prepare_save',
                ['department' => $departmentModel, 'request' => $this->getRequest()]
            );

            try {
                $departmentModel->save();
                $this->messageManager->addSuccess(__('Department saved'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $departmentModel->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the department'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}